package com.example.basicapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.basicapp.databinding.ActivityNextBinding

class NextActivity : AppCompatActivity() {

	private lateinit var binding: ActivityNextBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
//		setContentView(R.layout.activity_next)

		binding = ActivityNextBinding.inflate(layoutInflater)
		setContentView(binding.root)

		binding.fabNext.setOnClickListener {
			startFinal()
		}

		binding.btnToast.setOnClickListener {
			makeToast("This is my toast.")
		}

		Log.i("NextActivity", "onCreate")
	}

	override fun onStart() {
		super.onStart()

		Log.i("NextActivity", "onStart")
	}

	override fun onResume() {
		super.onResume()

		Log.i("NextActivity", "onResume")
	}

	override fun onPause() {
		super.onPause()

		Log.i("NextActivity", "onPause")
	}

	override fun onStop() {
		super.onStop()

		Log.i("NextActivity", "onStop")
	}

	override fun onDestroy() {
		super.onDestroy()

		Log.i("NextActivity", "onDestroy")
	}

	override fun onRestart() {
		super.onRestart()

		Log.i("NextActivity", "onRestart")
	}

	private fun startFinal() {
		startActivity(Intent(this, FinalActivity::class.java))
	}

	private fun makeToast(msg: String) {
		Toast.makeText(this, "This is a toast", Toast.LENGTH_SHORT).show()
	}

	fun functionName(): Int {
		val x = -1
		return when {
			x < 0 -> {
				//TODO:
				-1
			}
			x == 0 -> {
				Log.d("TAG", "print")
				0
			}
			x > 0 -> 1
			else -> 500
		}
	}
}